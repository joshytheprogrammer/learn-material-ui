import { Paper, Grid, Typography, Box, Rating } from '@mui/material';
import { AccessTime } from '@mui/icons-material'
import { createTheme, ThemeProvider } from '@mui/material';

const theme = createTheme({
    components: {
        MuiTypography: {
            variants: [
                {
                    props: {
                        variant: "body2",
                    },
                    style: {
                        fontSize: "11px",
                    }
                },
                {
                    props: {
                        variant: "body3",
                    },
                    style: {
                        fontSize: "9px",
                    }
                },
            ]
        }
    }
    
})

function TourCard() {
  return (
    <Grid item xs={3}>
        <ThemeProvider theme={theme}>
            <Paper elevation="3">
                <img src="https://images.unsplash.com/photo-1646257043001-47471d72dc5f?auto=format&fit=crop&w=500&q=60" alt="just-an-image"
                className='img' />
                <Box paddingX={1}>
                    <Typography variant='subtitle1' component='h2'>
                        Immerse into the Falls
                    </Typography>
                    <Box 
                        sx={{
                            display: "flex",
                            alignItems: "center"
                        }}
                    >
                        <AccessTime sx={{ width: '12.5px'}} />
                        <Typography variant="body2" component="p" marginLeft={0.5}> 5 hours</Typography>
                    </Box>
                    <Box
                        sx={{
                            display: "flex",
                            alignItems: "center" 
                        }}
                        marginTop={3}
                    >
                        <Rating name="read-only" value={4.5} precision={0.5}  readOnly size="small" />
                        <Typography variant="body2" component="p" marginLeft={0.5} >(4.5)</Typography>
                        <Typography variant="body3" component="p" marginLeft={0.5} >(655 reviews)</Typography>
                    </Box>
                    <Box>
                        <Typography vairant="h6" compontent="p">From C $147</Typography>
                    </Box>
                </Box>
            </Paper>
        </ThemeProvider>
    </Grid>
    
  )
}

export default TourCard